import * as bodyParser from "body-parser";
import * as cors from "cors";
import * as express from "express";
import * as http from "http";
import * as path from "path";
import "reflect-metadata";
import { FactoryDB } from "./database/FactoryDb";
import { Routes } from "./routes";
import UtilService from "./util/UtilService";
import { ConnDB, Folder } from "./util/Values";

const port: number = normalizePort(parseInt(process.env.PORT) || 8080);
// const port:number = normalizePort(8080);

try {
    
    Folder.SRC = __dirname;

    UtilService.setVariablesEnv();

    const app = express();//Criando a aplicação.

    middleware(app);

    createRoutes(app);
    
    app.set('port', port);
    
    let factoryDb: FactoryDB = new FactoryDB();
    
    
    const server = http.createServer(app);
    
    server.listen(port, "0.0.0.0", async () => {
        console.log(`---------- Server run in port: ${port}`);
        //Create connection Postgres
        await factoryDb.createConnectionPostgres(ConnDB.CONN_MAIN);
    });

    server.on('error', onError);
    process.once("SIGINT", async () => {
        await factoryDb.closeConnectionPostgres(() => {
            console.log("----------------- Connnection database closed!");
            console.log("----------------- System reboot!");
            process.exit(0);
        }, () => {
            console.log("----------------- Error on close connnection database!");
            console.log("----------------- System reboot!");
            process.exit(0);
        });
    });

    process.once("SIGUSR2", async () => {
        await factoryDb.closeConnectionPostgres(() => {
            console.log("----------------- Connnection database closed!");
            console.log("----------------- System reboot!");
            process.kill(process.pid, "SIGUSR2");
        }, () => {
            console.log("----------------- Error on close connnection database!");
            console.log("----------------- System reboot!");
            process.kill(process.pid, "SIGUSR2");
        });
    });
} catch (error) {
    console.log(error);
}



// *********************************** Functions Auxilary
function middleware(exp: express.Application): void {
    exp.use(bodyParser.json());
    exp.use(bodyParser.urlencoded({ extended: true }));
    console.log()
    exp.use(express.static(path.join(__dirname, "../public")))
    exp.use(cors());
}

function createRoutes(exp: express.Application) {
    Routes.forEach(route => {
        (exp as any)[route.method](route.route, (req: express.Request, res: express.Response, next: Function) => {
            // console.log(`-------------- ${route}`);
            const result = (new (route.controller as any))[route.action](req, res, next);
            if (result instanceof Promise) {
                result.then(result => result !== null && result !== undefined ? res.send(result) : undefined);

            } else if (result !== null && result !== undefined) {
                res.json(result);
            }
        });
    });
}


function normalizePort(val: number): number {
    let port: number = (typeof val === 'string') ? parseInt(val, 10) : val;
    if (isNaN(port)) return val;
    else if (port >= 0) return port;
    else return port;
}

function onError(error: NodeJS.ErrnoException): void {
    if (error.syscall !== 'listen') throw error;
    let bind = (typeof port === 'string') ? 'Pipe ' + port : 'Port ' + port;
    switch (error.code) {
        case 'EACCES':
            console.error(`${bind} requires elevated privileges`);
            process.exit(1);
            break;
        case 'EADDRINUSE':
            console.error(`${bind} is already in use`);
            process.exit(1);
            break;
        default:
            throw error;
    }
}

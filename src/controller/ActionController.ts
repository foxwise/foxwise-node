import { NextFunction, Request, Response } from "express";
import { getRepository } from "typeorm";
import { DataTypeOrm } from "../database/DataTypeOrm";
import { Action } from "../entity/Action";
import { HttpStatus } from "../enum/HtppStatus";
import { MsgAction, MsgGeneric } from "../enum/MessageTranslate";
import { TypeMSG } from "../enum/TypeMSG";
import { ConnDB } from "../util/Values";

export class ActionController {
    private ActionRepository = getRepository(Action, ConnDB.CONN_MAIN);

    async one(request: Request, response: Response, next: NextFunction) {
        let data = new DataTypeOrm();
        let idAction = request.params.id;
        try {
            data.validAddError(!idAction, MsgGeneric.idNotSet);
            if(!data.hasErrorSend(HttpStatus.BAD_REQUEST, response)){
                data.obj = await this.ActionRepository.findOne({"id": idAction});
                if(data.obj["id"]){
                    data.send(HttpStatus.OK, response);
                }else{
                    data.hasErrorSend(HttpStatus.NOT_FOUND, response, TypeMSG.DANGER, MsgGeneric.notRecordsFound);
                }
            }

        } catch (error) {
            data.send(HttpStatus.INTERNAL_SERVER_ERROR, response, TypeMSG.DANGER, MsgGeneric.errorFindRecord, error);
            
        }
    }

    async all(request: Request, response: Response, next: NextFunction) {
        let data = new DataTypeOrm();
        try {
           await data.findFilter(request, response, this.ActionRepository);
           data.send(HttpStatus.OK, response);
        } catch (error) {
            data.send(HttpStatus.INTERNAL_SERVER_ERROR, response, TypeMSG.DANGER, MsgGeneric.errorListRecords, error);
        }
    }
    
    
    async save(request: Request, response: Response, next: NextFunction) {
        let data = new DataTypeOrm();
        let actionRequest: Action = request.body;
        try {
            data.validAddError(!actionRequest.url, MsgAction.urlActionRequired);
            data.validAddError(!actionRequest.method, MsgAction.methodActionRequired);
            data.validAddError(!actionRequest.name, MsgAction.nameActionRequired);
            if(!data.hasErrorSend(HttpStatus.BAD_REQUEST, response)){
                let existAction = await this.ActionRepository.findOne({"url": actionRequest.url, "method": actionRequest.method});
                data.validAddError(existAction, MsgAction.methodUrlActionAlereadyExists);
                if(!data.hasErrorSend(HttpStatus.BAD_REQUEST, response)){
                    data.obj = await this.ActionRepository.save(actionRequest);
                    data.send(HttpStatus.CREATED, response, TypeMSG.SUCCESS, MsgGeneric.recordInsertedSuccessfully)
                }
            }
        } catch (error) {
           data.send(HttpStatus.INTERNAL_SERVER_ERROR, response, TypeMSG.DANGER, MsgGeneric.errorInsertRecord, error);
        }
    }

    async update(request: Request, response: Response, next: NextFunction) {
        let data = new DataTypeOrm();
        let idAction = request.params.id;
        let actionRequest: Action = request.body;
        try {
            data.validAddError(!idAction, MsgGeneric.idNotSet);
            data.validAddError(!actionRequest.url, MsgAction.urlActionRequired);
            data.validAddError(!actionRequest.method, MsgAction.methodActionRequired);
            data.validAddError(!actionRequest.name, MsgAction.nameActionRequired);
            if(!data.hasErrorSend(HttpStatus.BAD_REQUEST, response)){
                let existAction = await this.ActionRepository.findOne(idAction, {"select": ["id"]});
                data.validAddError(!existAction, MsgGeneric.notRecordsFound);
                if(!data.hasErrorSend(HttpStatus.NOT_FOUND, response)){
                    let existMethodUrlAction = await this.ActionRepository.findOne({"url": actionRequest.url, "method": actionRequest.method});
                    data.validAddError(existMethodUrlAction && existMethodUrlAction.id != idAction, MsgAction.methodUrlActionAlereadyExists);
                    if(!data.hasErrorSend(HttpStatus.BAD_REQUEST, response)){
                        existAction = await this.ActionRepository.merge(existAction, actionRequest);
                        data.obj = await this.ActionRepository.save(existAction);
                        data.send(HttpStatus.CREATED, response, TypeMSG.SUCCESS, MsgGeneric.recordAlteredSuccessfully)
                    }
                }
            }
        } catch (error) {
           data.send(HttpStatus.INTERNAL_SERVER_ERROR, response, TypeMSG.DANGER, MsgGeneric.errorAlterRecord, error);
        }
    }

    async remove(request: Request, response: Response, next: NextFunction) {
        let data = new DataTypeOrm();
        let idAction = request.params.id;
        try {
            data.validAddError(!idAction, MsgGeneric.idNotSet);
            if(!data.hasErrorSend(HttpStatus.BAD_REQUEST, response)){
                let ActionToRemove = await this.ActionRepository.findOne(request.params.id, {"select":["id"]});
                data.validAddError(!ActionToRemove, MsgGeneric.recordNotExist);
                if(!data.hasErrorSend(HttpStatus.NOT_FOUND, response)){
                    data.obj = await this.ActionRepository.remove(ActionToRemove);
                    data.send(HttpStatus.OK, response, TypeMSG.SUCCESS, MsgGeneric.recordRemovedSuccessfully);
                }
            }
        } catch (error) {
            data.send(HttpStatus.INTERNAL_SERVER_ERROR, response, TypeMSG.DANGER, MsgGeneric.errorRemoveRecord, error);
        }
    }
}
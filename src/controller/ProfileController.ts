import { NextFunction, Request, Response } from "express";
import { getRepository, In } from "typeorm";
import { DataTypeOrm } from "../database/DataTypeOrm";
import { Branch } from "../entity/Branch";
import { Profile } from "../entity/Profile";
import { HttpStatus } from "../enum/HtppStatus";
import { MsgAction, MsgBranch, MsgGeneric, MsgProfile } from "../enum/MessageTranslate";
import { TypeMSG } from "../enum/TypeMSG";
import { ConnDB } from "../util/Values";
import { Action } from "../entity/Action";

export class ProfileController {
    private profileRepository = getRepository(Profile, ConnDB.CONN_MAIN);
    private branchRepository = getRepository(Branch, ConnDB.CONN_MAIN);

    async one(request: Request, response: Response, next: NextFunction) {
        let data = new DataTypeOrm();
        let idProfile = request.params.id;
        try {
            data.validAddError(!idProfile, MsgGeneric.idNotSet);
            if(!data.hasErrorSend(HttpStatus.BAD_REQUEST, response)){
                data.obj = await this.profileRepository.findOne(idProfile, {
                    "relations": ["branch", "actions"]
                });
                if(data.obj["id"]){
                    data.send(HttpStatus.OK, response);
                }else{
                    data.hasErrorSend(HttpStatus.NOT_FOUND, response, TypeMSG.DANGER, MsgGeneric.notRecordsFound);
                }
            }

        } catch (error) {
            data.send(HttpStatus.INTERNAL_SERVER_ERROR, response, TypeMSG.DANGER, MsgGeneric.errorFindRecord, error);
            
        }
    }

    async all(request: Request, response: Response, next: NextFunction) {
        let data = new DataTypeOrm();
        try {
            data.joinEntity("branch", ["id", "fantasy", "cnpj"]);
           await data.findFilter(request, response, this.profileRepository);
           data.send(HttpStatus.OK, response);
        } catch (error) {
            data.send(HttpStatus.INTERNAL_SERVER_ERROR, response, TypeMSG.DANGER, MsgGeneric.errorListRecords, error);
        }
    }
    
    
    async save(request: Request, response: Response, next: NextFunction) {
        let data = new DataTypeOrm();
        let profileRequest: Profile = request.body;
        try {
            data.validAddError(!profileRequest.name, MsgProfile.nameProfileRequired);
            data.validAddError(!profileRequest.branch || !profileRequest.branch.id, MsgBranch.branchRequired);
            if(!data.hasErrorSend(HttpStatus.BAD_REQUEST, response)){
                let branch: Branch = await this.branchRepository.findOne(profileRequest.branch.id, {"select": ["id"]});
                data.validAddError(!branch, MsgBranch.branchNotExist);
                if(!data.hasErrorSend(HttpStatus.NOT_FOUND, response)){
                    data.validAddError(!profileRequest.actions && profileRequest.actions.length <= 0, MsgAction.actionRequired);
                    if(!data.hasErrorSend(HttpStatus.BAD_REQUEST, response)){
                        data.obj = await this.profileRepository.save(profileRequest);
                        data.send(HttpStatus.CREATED, response, TypeMSG.SUCCESS, MsgGeneric.recordInsertedSuccessfully)
                    }
                }
            }
        } catch (error) {
           data.send(HttpStatus.INTERNAL_SERVER_ERROR, response, TypeMSG.DANGER, MsgGeneric.errorInsertRecord, error);
        }
    }

    async update(request: Request, response: Response, next: NextFunction) {
        let data = new DataTypeOrm();
        let idProfile = request.params.id;
        let profileRequest: Profile = request.body;
        try {
            data.validAddError(!idProfile, MsgGeneric.idNotSet);
            data.validAddError(!profileRequest.name, MsgProfile.nameProfileRequired);
            data.validAddError(!profileRequest.branch || !profileRequest.branch.id, MsgBranch.branchRequired);
            if(!data.hasErrorSend(HttpStatus.BAD_REQUEST, response)){
                let profileExist: Profile = await this.profileRepository.findOne(idProfile, {"select": ["id"]});
                data.validAddError(!profileExist, MsgGeneric.recordNotExist);
                if(!data.hasErrorSend(HttpStatus.NOT_FOUND, response)){
                    let branch: Branch = await this.branchRepository.findOne(profileRequest.branch.id, {"select": ["id"]});
                    data.validAddError(!branch, MsgBranch.branchNotExist);
                    if(!data.hasErrorSend(HttpStatus.NOT_FOUND, response)){
                        data.validAddError(!profileRequest.actions || profileRequest.actions.length <= 0, MsgAction.actionRequired);
                        if(!data.hasErrorSend(HttpStatus.BAD_REQUEST, response)){
                            profileExist = await this.profileRepository.merge(profileExist, profileRequest);
                            data.obj = await this.profileRepository.save(profileExist);
                            data.send(HttpStatus.CREATED, response, TypeMSG.SUCCESS, MsgGeneric.recordAlteredSuccessfully)
                        }
                    }
                }
            }
        } catch (error) {
           data.send(HttpStatus.INTERNAL_SERVER_ERROR, response, TypeMSG.DANGER, MsgGeneric.errorAlterRecord, error);
        }
    }

    async remove(request: Request, response: Response, next: NextFunction) {
        let data = new DataTypeOrm();
        let idProfile = request.params.id;
        try {
            data.validAddError(!idProfile, MsgGeneric.idNotSet);
            if(!data.hasErrorSend(HttpStatus.BAD_REQUEST, response)){
                let ProfileToRemove: Profile = await this.profileRepository.findOne(idProfile, {"select":["id"]});
                data.validAddError(!ProfileToRemove, MsgGeneric.recordNotExist);
                if(!data.hasErrorSend(HttpStatus.NOT_FOUND, response)){
                    data.obj = await this.profileRepository.remove(ProfileToRemove);
                    data.send(HttpStatus.OK, response, TypeMSG.SUCCESS, MsgGeneric.recordRemovedSuccessfully);
                }
            }
        } catch (error) {
            data.send(HttpStatus.INTERNAL_SERVER_ERROR, response, TypeMSG.DANGER, MsgGeneric.errorRemoveRecord, error);
        }
    }
}
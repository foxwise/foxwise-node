import { NextFunction, Request, Response } from "express";
import { getRepository } from "typeorm";
import { DataTypeOrm } from "../database/DataTypeOrm";
import { Company } from "../entity/Company";
import { HttpStatus } from "../enum/HtppStatus";
import { MsgGeneric, MsgCompany } from "../enum/MessageTranslate";
import { TypeMSG } from "../enum/TypeMSG";
import Validation from "../util/Validation";
import { ConnDB } from "../util/Values";

export class CompanyController {
    private CompanyRepository = getRepository(Company, ConnDB.CONN_MAIN);

    async one(request: Request, response: Response, next: NextFunction) {
        let data = new DataTypeOrm();
        let idCompany = request.params.id;
        try {
            data.validAddError(!idCompany, MsgGeneric.idNotSet);
            if(!data.hasErrorSend(HttpStatus.BAD_REQUEST, response)){
                data.obj = await this.CompanyRepository.findOne({
                    "where":[{"id": idCompany}],
                    "relations": ["branchs"]
                 });
                if(data.obj["id"]){
                    data.send(HttpStatus.OK, response);
                }else{
                    data.hasErrorSend(HttpStatus.NOT_FOUND, response, TypeMSG.DANGER, MsgGeneric.notRecordsFound);
                }
            }

        } catch (error) {
            data.send(HttpStatus.INTERNAL_SERVER_ERROR, response, TypeMSG.DANGER, MsgGeneric.errorFindRecord, error);
            
        }
    }

    async all(request: Request, response: Response, next: NextFunction) {
        let data = new DataTypeOrm();
        try {
           data.joinEntity("branchs", ["id","fantasy", "cnpj"]);
           await data.findFilter(request, response, this.CompanyRepository);
           data.send(HttpStatus.OK, response);
        } catch (error) {
            console.log(error);
            data.send(HttpStatus.INTERNAL_SERVER_ERROR, response, TypeMSG.DANGER, MsgGeneric.errorListRecords, error);
        }
    }
    
    async save(request: Request, response: Response, next: NextFunction) {
        let data = new DataTypeOrm();
        let companyRequest: Company = request.body;
        try {
            data.validAddError(!companyRequest.fantasy, MsgCompany.fantasyCompanyRequired);
            if(!data.hasErrorSend(HttpStatus.BAD_REQUEST, response)){
                let existCompany = await this.CompanyRepository.findOne({"fantasy": companyRequest.fantasy});
                data.validAddError(existCompany, MsgCompany.fantasyAlreadyRegistered);
                if(!data.hasErrorSend(HttpStatus.BAD_REQUEST, response)){
                    data.obj = await this.CompanyRepository.save(companyRequest);
                    data.send(HttpStatus.CREATED, response, TypeMSG.SUCCESS, MsgGeneric.recordInsertedSuccessfully)
                }
            }
        } catch (error) {
           data.send(HttpStatus.INTERNAL_SERVER_ERROR, response, TypeMSG.DANGER, MsgGeneric.errorInsertRecord, error);
        }
    }

    async update(request: Request, response: Response, next: NextFunction) {
        let data = new DataTypeOrm();
        let idCompany = request.params.id;
        let companyRequest: Company = request.body;
        try {
            data.validAddError(!idCompany, MsgGeneric.idNotSet);
            // data.validAddError(!companyRequest.url, MsgCompany.urlCompanyRequired);
            // data.validAddError(!companyRequest.method, MsgCompany.methodCompanyRequired);
            data.validAddError(!companyRequest.fantasy, MsgCompany.fantasyCompanyRequired);
            if(!data.hasErrorSend(HttpStatus.BAD_REQUEST, response)){
                let existCompany: Company = await this.CompanyRepository.findOne(idCompany, {"select": ["id"]});
                data.validAddError(!existCompany, MsgGeneric.notRecordsFound);
                if(!data.hasErrorSend(HttpStatus.NOT_FOUND, response)){
                    let existCompanyFantasy = await this.CompanyRepository.findOne({"fantasy": companyRequest.fantasy});
                    data.validAddError(existCompanyFantasy && existCompanyFantasy.id != idCompany, MsgCompany.fantasyAlreadyRegistered);
                    if(!data.hasErrorSend(HttpStatus.BAD_REQUEST, response)){
                        let merge = await this.CompanyRepository.merge(existCompany, companyRequest);
                        data.obj = await this.CompanyRepository.save(merge);
                        data.send(HttpStatus.CREATED, response, TypeMSG.SUCCESS, MsgGeneric.recordAlteredSuccessfully)
                    }
                }
            }
        } catch (error) {
           data.send(HttpStatus.INTERNAL_SERVER_ERROR, response, TypeMSG.DANGER, MsgGeneric.errorAlterRecord, error);
        }
    }

    async remove(request: Request, response: Response, next: NextFunction) {
        let data = new DataTypeOrm();
        let idCompany = request.params.id;
        try {
            data.validAddError(!idCompany, MsgGeneric.idNotSet);
            if(!data.hasErrorSend(HttpStatus.BAD_REQUEST, response)){
                let CompanyToRemove: Company = await this.CompanyRepository.findOne(idCompany, {"relations": ["branchs"]});
                data.validAddError(!CompanyToRemove, MsgGeneric.recordNotExist);
                if(!data.hasErrorSend(HttpStatus.NOT_FOUND, response)){
                    data.validAddError(CompanyToRemove.branchs && CompanyToRemove.branchs.length > 0, MsgCompany.companyHasBranchs);
                    if(!data.hasErrorSend(HttpStatus.BAD_REQUEST, response)){
                        data.obj = await this.CompanyRepository.remove(CompanyToRemove);
                        data.send(HttpStatus.OK, response, TypeMSG.SUCCESS, MsgGeneric.recordRemovedSuccessfully);
                    }
                }
            }
        } catch (error) {
            data.send(HttpStatus.INTERNAL_SERVER_ERROR, response, TypeMSG.DANGER, MsgGeneric.errorRemoveRecord, error);
        }
    }
}
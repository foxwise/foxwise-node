export const ConnDB = {
    "CONN_MAIN"                                   : "",
    "LOCAL_POSTGRES"                              : "conn-postgres-local",
    "DEV_POSTGRES"                                : "conn-postgres-develop",
    "PROD_POSTGRES"                               : "conn-postgres-production",
    "TEST_POSTGRES"                               : "conn-postgres-test"
}

export const SecretToken ={
    "TOKEN"                                     : "x-access-token",
    "TIME"                                      :  84600,
    "SECRET"                                    : "fidelidade-node-key"
}

export const Folder = {
    "SRC": ""
}

export enum MsgGeneric  {
    "notRecordsFound"                       = "NOT_RECORDS_FOUND",
    "errorFindRecord"                       = "ERROR_FIND_RECORD",
    "errorListRecords"                      = "ERROR_LIST_RECORDS",
    "errorInsertRecord"                     = "ERROR_INSERT_RECORD",
    "errorAlterRecord"                      = "ERROR_ALTER_RECORD",
    "errorRemoveRecord"                     = "ERROR_REMOVE_RECORD",
    "recordInsertedSuccessfully"            = "RECORD_INSERTED_SUCCESSFULLY",
    "recordAlteredSuccessfully"             = "RECORD_ALTERED_SUCCESSFULLY",
    "recordRemovedSuccessfully"             = "RECORD_REMOVED_SUCCESSFULLY",
    "notSetField"                           = "NOT_SET_FIELD",
    "recordAlreadyExists"                   = "RECORD_ALREADY_EXISTS",
    "recordNotExist"                        = "RECORD_NOT_EXIST",
    "idNotSet"                              = "ID_NOT_SET",
    "emailAlreadyRegistered"                = "EMAIL_ALREADY_REGISTERED",
    "cpfAlreadyRegistered"                  = "CPF_ALREADY_REGISTERED",
    "cnpjAlreadyRegistered"                 = "CNPJ_ALREADY_REGISTERED",
    "loginAlreadyRegistered"                = "LOGIN_ALREADY_REGISTERED",
    "cnpjInvalid"                           = "CNPJ_INVALID",
    "cpfInvalid"                            = "CPF_INVALID",
    "emailInvalid"                          = "EMAIL_INVALID"
}

export enum MsgAuth {//Mensagens de retornos
    "errorQueryLoginAccess"                 = "ERROR_QUERY_LOGIN_ACCESS",
    "notSendToken"                          = "NOT_SEND_TOKEN",
    "loginInvalid"                          = "LOGIN_INVALID",
    "loginNotSend"                          = "LOGIN_NOT_SEND",
    "tokenInvalid"                          = "TOKEN_INVALID",
    "loginPasswordInvalid"                  = "LOGIN_PASSWORD_INVALID",
    "errorValidToken"                       = "ERROR_VALID_TOKEN",
    "notLoginFound"                         = "NOT_LOGIN_FOUND",
    "acessDanied"                           = "ACESS_DANIED",
    "tokenGenerateSuccessfully"             = "TOKEN_GENERATE_SUCCESSFULLY"
};

export enum MsgUser {
    "nameUserRequired"                      = "NAME_USER_REQUIRED",
    "emailUserRequired"                     = "EMAIL_USER_REQUIRED",
    "loginUserRequired"                     = "LOGIN_USER_REQUIRED",
    "passwordUserRequired"                  = "PASSWORD_USER_REQUIRED",
    "CPFUserRequired"                       = "CPF_USER_REQUIRED",
}

export enum MsgCompany {
    "fantasyCompanyRequired"                = "FANTASY_COMPANY_REQUIRED",
    "socialReasonCompanyRequired"           = "SOCIAL_REASON_COMPANY_REQUIRED",
    "fantasyAlreadyRegistered"              = "FANTASY_ALREADY_REGISTERED",
    "companyNotFound"                       = "COMPANY_NOT_FOUND",
    "companyNotExist"                       = "COMPANY_NOT_EXIST",
    "companyHasBranchs"                     = "COMPANY_HAS_BRANCHS",
}

export enum MsgBranch {
    "fantasyBranchRequired"                 = "FANTASY_BRANCH_REQUIRED",
    "branchRequired"                        = "BRANCH_REQUIRED",
    "socialReasonBranchRequired"            = "SOCIAL_REASON_BRANC_REQUIRED",
    "fantasyAlreadyRegistered"              = "FANTASY_ALREADY_REGISTERED",
    "branchNotExist"                        = "BRANCH_NOT_EXIST",
}

export enum MsgAction {
    "methodActionRequired"                  = "METHOD_ACTION_REQUIRED",
    "nameActionRequired"                    = "NAME_ACTION_REQUIRED",
    "urlActionRequired"                     = "URL_ACTION_REQUIRED",
    "actionRequired"                        = "ACTION_REQUIRED",
    "methodUrlActionAlereadyExists"         = "METHOD_URL_ACTION_ALREADY_EXISTS",
    "actionAlreadyExist"                    = "ACTION_NOT_EXIST",
}

export enum MsgProfile {
    "nameProfileRequired"                   = "NAME_PROFILE_REQUIRED",
    "profileRequired"                       = "PROFILE_REQUIRED",
    "profileNotExist"                       = "PROFILE_NOT_EXIST"
}
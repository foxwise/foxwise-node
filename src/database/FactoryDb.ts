import { Connection, ConnectionOptions, createConnection, getConnectionOptions } from "typeorm";
import { ConnDB, Folder } from "../util/Values";
export class FactoryDB {
    constructor(){}
    private mainConnectionPostgres: Connection;
    public async createConnectionPostgres(nameConnection: string): Promise<Connection>{
        try {
            console.log("--------------- Init Connection DataBase");
            // let connections: ConnectionOptions = await getConnectionOptions();
            let options: ConnectionOptions = await getConnectionOptions(nameConnection);
            // Acionando as configurações de produção
            options.entities.push(Folder.SRC + "/entity/**/*.js");
            options.migrations.push(Folder.SRC + "/migrations/**/*.js");
            options.subscribers.push(Folder.SRC + "/subscribers/**/*.js");
            console.log(options);
            this.mainConnectionPostgres = await createConnection(options);
            console.log(`Connection successuly: ${nameConnection}!`);
            return this.mainConnectionPostgres;
        } catch (error) {
            console.log(`Error on try connect in: ${ConnDB.CONN_MAIN} \n ${error}`);
        }
    }
    
    public async closeConnectionPostgres(onClose: Function, onError: Function){
        console.log("Try Close Connect");
        try {
            await this.mainConnectionPostgres.close();
            onClose();
        } catch (error) {
            onError();
        }

    }
}